DELETE FROM todo;
INSERT INTO todo (title, description, done)
    VALUES ('Gör klart 1', 'Beskrivning 1', false),
           ('Gör klart 2', 'Beskrivning 2', false),
           ('Gör klart 3', 'Beskrivning 3', false);
